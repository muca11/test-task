<?php

require_once("vendor/autoload.php");

class MyWorker extends Worker
{
    
    private $provider;

    public function __construct(DataProvider $provider)
    {
        $this->provider = $provider;
    }
    
    public function getProvider()
    {
        return $this->provider;
    }
}