<?php

require_once("vendor/autoload.php");


class DataProvider extends Threaded
{
    
    private $total;

    private $feeds = [];
    
    private $processed = 0;

    function __construct($feeds) {
        $this->feeds = $feeds;
        $this->total = count($feeds);
    }

    public function getNext()
    {
        if ($this->processed === $this->total) {
            return null;
        }

        $feeds = $this->feeds[$this->processed];

        $this->processed++;

        return $feeds;
    }
}