<?php

return [
	[
		"exchangeName" => "CoinDesk",
		"url" => "http://api.coindesk.com/v1/bpi/currentprice.json",
		"jsonPath" => ["bpi","USD","rate_float"]
	],
	[
		"exchangeName" => "Coinbase",
		"url" => "https://coinbase.com/api/v1/currencies/exchange_rates",
		"jsonPath" => ["btc_to_usd"]
	],
];
