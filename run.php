<?php

$threads = 2;

$feeds = include('feeds.php');

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once("vendor/autoload.php");

require_once 'MyWorker.php';
require_once 'Work.php';
require_once 'DataProvider.php';

$provider = new DataProvider($feeds);

$pool = new Pool($threads, 'MyWorker', [$provider]);

$start = microtime(true);


for ($i = 0; $i < $threads; $i++) {
    $pool->submit(new Work());
}

$pool->shutdown();