(function(angular) {
  'use strict';
var myApp = angular.module('myApp', []);

myApp.controller('MainController', ['$scope', function($scope) {
    $scope.activeTitle = '';
    $scope.coef = 1.11845; //EUR TO USD
    $scope.rate = 0;
    $scope.feeds = [];

    /**
     * Init function
     */
    $scope.init = function(timestamp){

        var queryString = {'timestamp' : timestamp};

    	$.ajax({
            type: 'GET',
            url: '/polling.php',
            data: queryString,
            success: function(data){
                var obj = JSON.parse(data);
                $scope.feeds = JSON.parse(obj.data_from_file);
                $scope.drawData();
                $scope.init(obj.timestamp);
            },
            error: function(data){
                $scope.init(timestamp);
            }
        });

    }

    $scope.$watch('activeTitle', function(){
        if($scope.activeTitle){
            $scope.drawData();
        }
    });


    /**
     * Draws a data.
     */
    $scope.drawData = function(){
        if($scope.activeTitle && $scope.feeds.length){
            $.each($scope.feeds, function(key,value){
                if($scope.activeTitle == value.exchangeName){
                    $scope.rate = value.rate;
                }
            });
        }
    }


    $scope.init();

}]);
})(window.angular);
