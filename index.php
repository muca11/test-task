<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body ng-app="myApp">

	<div class="container" ng-controller="MainController">
	    <div class="row">
	        <div class="col-md-5 col-md-offset-3">
	            <div class="panel panel-primary">
	                <div class="panel-heading">
	                     <h3 class="panel-title">
	                     	Bitcoin realtime ticker
	                     </h3>
	                </div>
	                <div class="panel-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">
                            	Feeds
                            </label>
                            <div class="col-sm-9">
                                <select class="form-control" ng-model="activeTitle">
                                	<option value="" disabled>Please select one</option>
                                	<option ng-repeat="item in feeds" value="{{item.exchangeName}}">{{item.exchangeName}}</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">BTC/USD</label>
                            <div class="col-sm-9">
                                {{rate | number: 2}}
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">EUR/USD</label>
                            <div class="col-sm-9">
                                {{coef}}
                            </div>
                            <div class="clearfix"></div>
                        </div>


                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">BTC/EUR</label>
                            <div class="col-sm-9">
                                {{rate / coef | number: 2}}
                            </div>
                            <div class="clearfix"></div>
                        </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
	<script src="/app.js"></script>

</body>
</html>