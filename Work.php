<?php

require_once("vendor/autoload.php");

class Work extends Threaded
{

    public function run()
    {
        do {
            $value = null;

            $provider = $this->worker->getProvider();

            // Sync data
            $provider->synchronized(function($provider) use (&$value) {
               $value = $provider->getNext();
            }, $provider);

            if ($value === null)
                continue;

            $json = file_get_contents($value["url"]);
            $obj = json_decode($json);

            $rate = $this->getDataFromObject($obj, $value["jsonPath"]);

            $data = file_get_contents('./data.json');
            $data = json_decode($data, true);

            $rewrite = true;

            if($data){
                foreach ($data as $key => $item) {
                    if($item['exchangeName'] == $value["exchangeName"]){
                        if($item['rate'] != $rate){
                            $rewrite = true;
                            unset($data[$key]);
                            break;
                        }
                        $rewrite = false;
                        break;
                    }
                }
            }else{
                $data = [];
            }

            if($rewrite){
                $obj = [];
                $obj["exchangeName"] = $value["exchangeName"];
                $obj["rate"] = $rate;
                array_push($data, $obj);

                file_put_contents('./data.json', json_encode(array_values($data)));
            }

        }
        while ($value !== null);
    }

    private function getDataFromObject($obj, $path){
        $return = null;

        foreach ($path as $key => $value) {
            try {
                if($return){
                    $return = $return->{$value};
                }else{
                    $return = $obj->{$value};
                }
            } catch (Exception $e) {
                echo 'Wrong json path: ',  $e->getMessage(), "\n";
            }
        }

        return $return;
    }

}