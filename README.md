# Front #
AngularJS front end and Long poll requests.
# Back #
Back end multithreading
# Requirements #
The application requires https://github.com/krakjoe/pthreads.git PHP extension.
pthreads-polyfill can be used to bypass multithreading.
# Configuration #
The application can be configured in feeds.php file.
Amount of threads can be configured in the run.php file by changing value of $threads variable.
# Installing #
Run `composer install` to install all required dependencies before using the application.
To fetch new data a cron job task ir required to launch run.php file